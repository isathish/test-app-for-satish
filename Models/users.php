<?php

include('../db/db_connection.php');

class User
{
    public function insertUser($data) {
        $db_conn = new Db_conn();
        $db = $db_conn->getConnection();

        $data = Array ("id" => isset($data['id']) ? $data['id'] : '',
                       "email" => $data['email'],
                       "password" => $data['password'],
                       "domain" => $data['domain']
        );
        $id = $db->insert('users', $data);
        if($id){
          return json_encode(array(
              'code' => '200',
              'message' => 'success',
              'data' => array(
                  'id' => $id
              )
          ));
        }
        else{
          return json_encode(array(
              'code' => '203',
              'message' => 'error'
          ));
        }

    }

    public function deleteUser($data) {
        $db_conn = new Db_conn();
        $db = $db_conn->getConnection();

        $db->where('id', $data['id']);
        $id = $db->delete('users');
        if($id){
          echo json_encode(array(
              'code' => '200',
              'message' => 'success',
              'data' => array(
                  'id' => $id
              )
          ));
        }
        else{
          echo json_encode(array(
              'code' => '203',
              'message' => 'error'
          ));
        }
    }

    public function getUsers() {
        $db_conn = new Db_conn();
        $db = $db_conn->getConnection();

        $users = $db->get('users');
        if($users){
          echo json_encode(array(
              'code' => '200',
              'message' => 'success',
              'data' => array(
                  'users' => $users
              )
          ));
        }
        else{
          echo json_encode(array(
              'code' => '200',
              'message' => 'success',
              'data' => array(
                  'users' => array()
              )
          ));
        }
    }
    
    public function getUsersByName($name){
        $db_conn = new Db_conn();
        $db = $db_conn->getConnection();

        $db->where('domain', $name);
        $users = $db->get('users');
        
        return $users;
    }
}

?>
