<?php

include('../db/db_connection.php');

class Site
{
    public function insertSite($data) {
        $db_conn = new Db_conn();
        $db = $db_conn->getConnection();

        $data = Array ("id" => isset($data['id']) ? $data['id'] : '',
                       "name" => $data['name'] . '.sirajulhaq.com'
        );
        $id = $db->insert('sites', $data);
        if($id){
          return json_encode(array(
              'code' => '200',
              'message' => 'success',
              'data' => array(
                  'id' => $id
              )
          ));
        }
        else{
          return json_encode(array(
              'code' => '203',
              'message' => 'error'
          ));
        }

    }

    public function deleteSite($data) {
        $db_conn = new Db_conn();
        $db = $db_conn->getConnection();

        $db->where('id', $data['id']);
        $id = $db->delete('sites');
        if($id){
            return json_encode(array(
                'code' => '200',
                'message' => 'success',
                'data' => array(
                    'id' => $id
                )
            ));
        }
        else{
          return json_encode(array(
              'code' => '203',
              'message' => 'error'
          ));
        }
    }

    public function getSites() {
        $db_conn = new Db_conn();
        $db = $db_conn->getConnection();

        $sites = $db->get('sites');
        return $sites;
    }
}

?>
