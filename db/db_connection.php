<?php

include('../vendor/joshcam/mysqli-database-class/MysqliDb.php');

class Db_conn {

    private $db_connection;

    public function __construct() {
        $mysqli = new mysqli('localhost', 'root', 's0s1@123', 'backendapi');

        if(!$mysqli){
            echo ('error mysqli');exit;
        }

        $db_connection = new MysqliDb($mysqli);

        if(!$db_connection){
            echo ('error db conn');exit;
        }
    }

    public function getConnection(){
        return MysqliDb::getInstance();
    }
}
