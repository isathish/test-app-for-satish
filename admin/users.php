<?php
/*** File created by Siraj ***/
include('../models/users.php');
$subDomain = $_GET['domain'];
$user = new User();

$userList = $user->getUsersByName($subDomain);
?>
<html>
<head>
    <title>Admin Page (Users)</title>
    
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" />
    <link rel="stylesheet" href="http://bootsnipp.com/dist/bootsnipp.min.css?ver=7d23ff901039aef6293954d33d23c066" />
<!--    <link rel="stylesheet" href="../assets/css/toastr.css" />-->
    <link rel="stylesheet" href="../site/assets/css/style.css" />
    
<!--    <script src="./assets/js/jquery-3.1.1.min.js"></script>-->
<!--    <script src="./assets/js/toastr.js"></script>-->
</head>
<body>
	<div class="container">
        <div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-md-offset-2">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="">
								<h2 style="text-align:center;">Admin Page</h2>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
								<h2 style="text-align:center;">Domain: <?php echo $subDomain;?></h2>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="">
                           <div class="container" id="adminPage">
                              <div class="row">
                                 
                                 <div class="row" style="margin-top: 30px">
                                    <div class="col-md-6 col-md-offset-3">
                                       <h4 class="text-center">Users</h4>
                                        <?php if(sizeof($userList) > 0) { ?>
                                       <div class="previous-sites-container list-group items-container">
                                       <?php 
                                         foreach($userList as $user){ 
                                       ?>
                                          <div class="site list-group-item text-center"><?php echo $user['email'];?></div>
                                           <?php } ?>
                                       </div>
                                        <?php } else { ?>
                                       <div class="no-users text-center ng-hide">No Users</div>
                                        <?php } ?>
                                       <div></div>
                                       
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
