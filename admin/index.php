<?php
/*** File created by Siraj ***/

include('../models/sites.php');
$subDomain = $_SERVER['HTTP_HOST'];
$site = new Site();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    switch($_POST['action']){
        case 'delete':
            $site->deleteSite($_POST);
            break;
            
        case 'create':
            $site->insertSite($_POST);
            break;
    }
}

$siteList = $site->getSites();
?>
<html>
<head>
    <title>Admin Page</title>
    
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" />
    <link rel="stylesheet" href="http://bootsnipp.com/dist/bootsnipp.min.css?ver=7d23ff901039aef6293954d33d23c066" />
<!--    <link rel="stylesheet" href="../assets/css/toastr.css" />-->
    <link rel="stylesheet" href="../site/assets/css/style.css" />
    
<!--    <script src="./assets/js/jquery-3.1.1.min.js"></script>-->
<!--    <script src="./assets/js/toastr.js"></script>-->
</head>
<body>
	<div class="container">
    		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-md-offset-2">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="">
								<h2 style="text-align:center;">Admin Page</h2>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="">
                           <div class="container" id="adminPage">
                              <div class="row">
                                 <div class="col-lg-8 col-md-8 col-md-offset-2">
                                    <form action="" method="post">
                                       <label for="basic-url">Enter domain</label>
                                       <div class="input-group">
                                           <input type="hidden" name="action" value="create" />
                                           <input type="text" name="name" placeholder="yourdomain" required class="form-control"> 
                                           <span class="input-group-addon" id="basic-addon3">.sirajulhaq.com</span>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-xs-12 btn btn-success" style="margin-top: 20px">Create</button>
                                        </div>
                                    </form>
                                 </div>
                               </div>
                               <hr>
                               <div class="row">
                                  
                                 <div class="row" style="margin-top: 30px">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-md-offset-2">
                                       <h4 class="text-center">Sites</h4>
                                        <?php if(sizeof($siteList) > 0) { ?>
                                       <div class="previous-sites-container list-group items-container">
                                       <?php 
                                         foreach($siteList as $site){ 
                                             $siteUrl = '/php/admin/users.php?domain='.$site['name'];
                                       ?>
                                          <div class="site list-group-item text-center">
                                              <form action="" method="post">
                                                  <input type="hidden" name="action" value="delete" />
                                                  <input type="hidden" name="id" value="<?php echo $site['id'];?>" />
                                                 <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
                                                     <a href="<?php echo $siteUrl;?>"><?php echo $site['name']; ?></a>
                                                  </div>
                                                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                                     <button class="btn btn-delete btn-danger btn-icon">
                                                        <div class="delete-btn glyphicon glyphicon-trash"></div>
                                                     </button>
                                                  </div>
                                              </form>
                                          </div>
                                           <?php } ?>
                                       </div>
                                        <?php } else { ?>
                                       <div class="no-users text-center ng-hide">No sites</div>
                                        <?php } ?>
                                       
                                        
                                       <div></div>
                                       
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
